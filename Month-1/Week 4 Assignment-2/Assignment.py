import sqlite3

def connection():
    conn = sqlite3.connect('Company.db')
    conn.row_factory = sqlite3.Row
    return conn


# Function to create tables
def createtable():
    con=connection()
    cursor=con.cursor()
    cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")
    query = """CREATE TABLE EMPLOYEE(Name CHAR(25) NOT NULL,ID INT NOT NULL,Salary INT NOT NULL,Department_id INT NOT NULL)"""
    cursor.execute(query)
    cursor.execute("DROP TABLE IF EXISTS DEPARTMENTS")
    query1 = """CREATE TABLE DEPARTMENTS(Department_id INT NOT NULL,Department_name CHAR(25) NOT NULL)"""
    cursor.execute(query1)

# Function to insert column
def insert_column():
    cursor.execute("ALTER TABLE EMPLOYEE ADD CITY CHAR(30) NOT NULL")

# Function to insert 5 records into EMPLOYEE table
def insert_record():
    record = [ ' ', 0, 0, 0, ' ']
    for i in range(0,5):
        print(f"Enter details for Employee {i+1}\n")
        record[0]=input("Enter Name\n")
        record[1]=int(input("Enter ID\n"))
        record[2]=int(input("Enter Salary\n"))
        record[3]=int(input("Enter Department ID\n"))
        record[4]=input("Enter City\n")
        cursor.execute("INSERT INTO EMPLOYEE VALUES(?,?,?,?,?)",(record[0],record[1],record[2],record[3],record[4]))
    
# Function to insert 5 records into DEPARTMENTS table
def insert_record1():
    record1=[]
    for i in range(0,5):
        print(f"Enter details for Department Table {i+1}\n")
        record1[0]=int(input("Enter Department ID\n"))
        record1[1]=input("Enter Department Name\n")
        cursor.execute("INSERT INTO DEPARTMENTS VALUES(?,?)",(record1[0],record1[1]))

# Function to display EMPLOYEE Table details
def display():
    cursor.execute("SELECT Name, ID, Salary FROM EMPLOYEE")
    result=cursor.fetchall()
    if (len(result) == 0):
        print("Table is Empty")
    else:
        for row in result:
            print(row[0],row[1],row[2])

# Function to display Employee details by Department ID
def displaydep():
    id=int(input("Enter Department ID"))
    cursor.execute("SELECT * FROM Departments WHERE Department_ID=:id",{'id':id})
    results=cursor.fetchone()
    if results is not None:
        print(f'Employee Details of the Department {results[1]} are')
        cursor.execute("SELECT * FROM EMPLOYEE WHERE Department_id=:id", {'id': id})
        results=cursor.fetchall()
        for row in results:
            print(row[0],row[1],row[2],row[3],row[4])
    else:
        print("An Error Occured. No Department with th Inputted ID")


# Function to print the details of employees whose names start with user input letter
def letter():
    letter = input("\nEnter the First Letter of the Employee to display the name of the Employees starting with it:")
    cursor.execute("SELECT * FROM Employee WHERE Name LIKE '{}%'".format(letter))
    result=cursor.fetchall()
    if (len(result) == 0):
        print(f"An Error Occured! No Employee with First letter {letter}")
    else:
        for row in result:
            print(row[0],row[1],row[2],row[3],row[4])

# Function to print the details of employees with ID’s inputted by the user
def id():
    idnumber= input("\nEnter the ID of the Employee to display their details:")
    cursor.execute("SELECT * FROM Employee WHERE ID = {idnumber}")
    result=cursor.fetchall()
    if (len(result) == 0):
        print(f"An Error Occured! No Employee with ID {idnumber}")
    else:
        for row in result:
            print(row[0],row[1],row[2],row[3],row[4])


# Function to change the name of the employee whose ID is input by the user
def changename():
    idnum= input("\nEnter the ID of the Employee to change the name:")
    cursor.execute("SELECT * FROM Employee WHERE ID = {idnum}")
    result=cursor.fetchall()
    if (len(result) == 0):
        print(f"An Error Occured! No Employee with ID {idnum}")
    else:
        for row in result:
            print(row[0],row[1],row[2],row[3],row[4])
        name=input("\nEnter the new name to update")
        cursor.execute("UPDATE Employee SET Name='{}' WHERE ID='{}'".format(name,idnum))
        print("\nThe Updated Database is:")
        cursor.execute("SELECT * FROM EMPLOYEE")
        result = cursor.fetchall()
        for row in result:
            print(row[0],row[1],row[2],row[3],row[4])



# Main Program
con=connection()
cursor=con.cursor()
createtable()
option=input("Do you want to add a new column 'CITY' to the table EMPLOYEE (YES/NO)?\n").upper()
if(option=='YES'):
    insert_column()
print('Please insert 5 records into table EMPLOYEE\n')
insert_record()
option1=input("Do you want to display EMPLOYEE table details(YES/NO)?\n").upper()
if(option=='YES'):
    display()
letter()
id()
changename()
print('Please insert 5 records into table DEPARTMENTS\n')
insert_record1()
displaydep()
conn.commit()

import numpy as np
import random


# dictionary to store gamelog
history = dict()


# function to choose who plays first
def choose_turn():
    if random.randint(0, 1) == 0:
        return 'Computer'
    else:
        return 'Player' 


# displaying the game board    
def display_board(board):
    
    board_list = []

    # converts the numbers into their appropriate symbols
    # 0 -> O, 1 -> X, 3 -> ' '
    internal_arr = board.flatten()
    for i in range(0, 9):
       if internal_arr[i] == 0:
           board_list.append('O')
       elif internal_arr[i] == 1:
           board_list.append('X')
       else:
           board_list.append(' ')

    # printing the numbers as a board
    print("""
 {} | {} | {}
---+---+---
 {} | {} | {}
---+---+---
 {} | {} | {}

""".format(*board_list))


# assigning player and computer symbol at random
def player_input():
    if random.randint(0, 1) == 0:
        print("Player will play as X\n")
        return (1,0)
    else:
        print("Player will play as O\n")
        return (0,1)
    

# checking if there are open slots
def return_open_slots():
    open_slots = []

    bool_arr = (board_arr == 3)
    flat_bool_arr = bool_arr.flatten()
    
    # if the spot taken has value 3 then spot is open
    for i in range(0, len(flat_bool_arr)):
        if flat_bool_arr[i] == True:
            open_slots.append(i + 1)
            
    return open_slots


# function to print the results of the game
def game_result(last_played_num):
    if last_played_num == user_num:
        print("PLAYER WINS\n")
    elif last_played_num == comp_num:
        print("COMPUTER WINS\n")
    else:
        print("MATCH TIED\n")
        

# storing the log of each game
def store_history(gno, rno, board):
    history[gno][rno] = board[:]


# printing the details of each game
def print_history(history, index):
    for i in range(1, 10):
        if i in history[index].keys():
            print(f"\nRound-{i}")
            display_board(history[index][i])
    print(f"\n{history[index]['winner']} won Game-{index}")


# checks to see if there is a winner after each round
def check_for_winner(last_played_num):
    
    if return_open_slots == []:
    # Checks if no open slots
        game_result("Draw!")
        
    for i in range(0, 3):
    # Checks rows and columns for a result
        rows_win = (board_arr[i, :] == last_played_num).all()
        cols_win = (board_arr[:, i] == last_played_num).all()
        
        if rows_win or cols_win:
            game_result(last_played_num)
            return True
            
    diag1_win = (np.diag(board_arr) == last_played_num).all()
    diag2_win = (np.diag(np.fliplr(board_arr)) == last_played_num).all()
    
    # checking the diagonals for a result
    if diag1_win or diag2_win:
        game_result(last_played_num)
        return True



# MAIN PROGRAM
game_number = 1
print('Welcome to Tic Tac Toe!\n')
no_of_games = int(input("How many games do you want to play? "))

while game_number <= no_of_games:

    history[game_number] = {}
    turn = choose_turn()
    round_number = 0

    # Creating the gameboard. The number 3 is used to indicate empty space
    board_arr = np.array([[3, 3, 3],
                          [3, 3, 3],
                          [3, 3, 3]])

    # taking user and computer symbol
    user_num, comp_num = player_input()

    # main game logic
    while True:
        if turn == 'Player':
            round_number += 1

            print("Player turn")
            display_board(board_arr)

            while True:
                user_input = input("Pick an open slot: ")
                user_input = int(user_input)

                if user_input in return_open_slots():

                    # marking the position
                    temp_array = board_arr.flatten()
                    temp_array[user_input-1] = user_num
                    board_arr = temp_array.reshape(3,3)
                    break
                else:
                    print("That's not a open slot")

            store_history(game_number, round_number, board_arr)

            turn = 'Computer'      
            
            if check_for_winner(user_num):
                history[game_number]["winner"] = "Player"
                break

        else:
            round_number += 1
            print("\nComputer's Turn\n")

            open_slots = return_open_slots()
            comp_input = random.choice(open_slots)

            # marking the position
            temp_array = board_arr.flatten()
            temp_array[comp_input-1] = comp_num
            board_arr = temp_array.reshape(3,3)

            # storing round details into the game log
            store_history(game_number, round_number, board_arr)

            display_board(board_arr)

            turn = 'Player'

            if check_for_winner(comp_num):
                history[game_number]["winner"] = "Computer"
                break
    
    game_number+=1


# displaying each round information
while True:
    answer = int(input("\nEnter the round for which you need information. Enter 0 to quit\n"))
    if answer == 0:
        break
    try:
        print_history(history, answer)
    except:
        print("The round you searched for doesn't exist")

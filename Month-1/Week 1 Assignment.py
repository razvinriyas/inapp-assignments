from random import randint
choices =["Rock", "Paper", "Scissors"]
choices_dict = {"Rock": ["Paper", "Scissors"], "Paper": ["Scissors", "Rock"], "Scissors": ["Rock", "Paper"]}
userscore = 0
computerscore = 0
roundinfo = {}

for i in range (1, 11):
    print("\nRound {}".format(i))
    computer_choice = choices[randint(0, 2)]
    player_input = int(input("Enter your choice (0- Rock, 1- Paper, 2- Scissors): "))
    user_choice = choices[player_input]
    
    if user_choice == computer_choice:
        print("It's a tie!")
        round_winner = "Both"
    elif choices_dict[user_choice][1] == computer_choice:
        print("Player wins!")
        round_winner = "Player"
        userscore = userscore + 1
    elif choices_dict[user_choice][0] == computer_choice:
        print("Computer wins!")
        round_winner = "Computer"
        computerscore = computerscore + 1

    
    print(f"Your choice: {user_choice}, Computer choice: {computer_choice}")
    roundinfo[i]= [user_choice, computer_choice, round_winner]
    if userscore > computerscore:
        print("\n Player wins")
    elif userscore < computerscore:
        print("\n Computer wins")
    else:
        print("\nIt is tie")

    print(f"\nThe winner is {round_winner}!! \n")

while True:
    roundnum = int(input("\nEnter the round for which you need information. Enter 0 to quit\n"))
    if roundnum == 0:
        break
    try:
        print(f"\nPlayer choice = {roundinfo[roundnum][0]} \nComputer choice = {roundinfo[roundnum][1]} \n{roundinfo[roundnum][2]} won Round {roundnum}")
        
    except:
        print("The Round you searched for doesn't exist")
import sqlite3


def connection():
    conn=sqlite3.connect('database.sqlite')
    return conn


def question1b():
    con=connection()
    cursor=con.cursor()
    print("\nMATCHES PLAYED IN 2015 WITH FTHG = 5")
    print("\nHome Team \t\t\tAway Team")
    cursor.execute("Select HomeTeam,AwayTeam From Matches Where Season=2015 and FTHG=5")
    result=cursor.fetchall()
    for row in result:
        print(f'{row[0]}\t\t\t{row[1]}\n')
    con.commit()
    con.close()


def question1c():
    con=connection()
    cursor=con.cursor()
    print("\nMATCHES WHERE ARSENAL IS HOME TEAM & FTR = A")
    cursor.execute("Select * From Matches Where HomeTeam='Arsenal' and FTR='A' ")
    result=cursor.fetchall()
    print("\nMatch ID\tDiv \tSeason\tDate\t\tHome Team\tAway Team\tFTHG\tFTAG\tFTR")
    for row in result:
        print(f'{row[0]}\t\t{row[1]} \t{row[2]}\t{row[3]}\t{row[4]}\t\t{row[5]}\t\t{row[6]}\t{row[7]}\t{row[8]}\n')
    con.commit()
    con.close()


def question1d():
    con=connection()
    cursor=con.cursor()
    print("\nMATCHES FROM 2012 TO 2015 WHERE BAYERN MUNICH IS AWAY TEAM & FTAG > 2")
    cursor.execute("Select * From Matches Where Season Between 2012 and 2015 and AwayTeam='Bayern Munich' and FTHG>2")
    result=cursor.fetchall()
    print("\nMatch ID\tDiv \tSeason\tDate\t\tHome Team\tAway Team\tFTHG\tFTAG\tFTR")
    for row in result:
        print(f'{row[0]}\t\t{row[1]} \t{row[2]}\t{row[3]}\t{row[4]}\t{row[5]}\t{row[6]}\t{row[7]}\t{row[8]}\n')
    con.commit()
    con.close()


def question1e():
    con=connection()
    cursor=con.cursor()
    print("\nMATCHES WHERE HOME TEAM STARTS WITH A & AWAY TEAM WITH M")
    cursor.execute("Select * From Matches Where HomeTeam Like 'A%' and AwayTeam Like 'M%'")
    result=cursor.fetchall()
    print("\nMatch ID\tDiv \tSeason\tDate\t\tHome Team\tAway Team\tFTHG\tFTAG\tFTR")
    for row in result:
        print(f'{row[0]}\t\t{row[1]} \t{row[2]}\t{row[3]}\t{row[4]}\t{row[5]}\t{row[6]}\t{row[7]}\t{row[8]}\n')
    con.commit()
    con.close()


def question2a():
    con=connection()
    cursor=con.cursor()
    print("\nCOUNT ROWS IN MATCH TABLE")
    cursor.execute("Select Count(*) From Teams")
    result=cursor.fetchone()
    print(f"\nThe Count of all the rows in the Team Table are {result[0]}")
    con.commit()
    con.close()


def question2b():
    con=connection()
    cursor=con.cursor()
    print("\nDISTINCT SEASONS")
    cursor.execute("Select Distinct Season From Teams")
    result=cursor.fetchall()
    print("\nThe Unique Values that are included in the Season Column in the Team Table are")
    for row in result:
        print(row[0])
    con.commit()
    con.close()


def question2c():
    con=connection()
    cursor=con.cursor()
    print("\nSTADIUM CAPACITY")
    cursor.execute("Select Max(StadiumCapacity),Min(StadiumCapacity) From Teams")
    result=cursor.fetchall()
    print(f"\nThe Largest Stadium Capacity is {result[0][0]} and Minimum Stadium Capacity is {result[0][1]}")
    con.commit()
    con.close()


def question2d():
    con=connection()
    cursor=con.cursor()
    print("\nSUM OF PLAYERS FOR ALL TEAMS IN 2014")
    cursor.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season = 2014")
    result=cursor.fetchall()
    print(f"Total sum of squad players for all teams during the 2014 season is {result[0][0]}")
    con.commit()
    con.close()


def question2e():
    con=connection()
    cursor=con.cursor()
    print("\nAVG GOAL SCORE OF MAN UNITED DURING HOME GAMES")
    cursor.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam = 'Man United'")
    result=cursor.fetchall()
    print(result[0][0])
    con.commit()
    con.close()


def question3a():
    con=connection()
    cursor=con.cursor()
    print("\nMATCHES IN 2010 WHERE HOME TEAM IS AACHEN IN DESCENDING ORDER OF NUMBER OF HOME GOALS")
    cursor.execute("Select HomeTeam, FTHG, FTAG From Matches Where Season=2010 and HomeTeam='Aachen' Order By FTHG Desc")
    result=cursor.fetchall()
    print("\nHome Team\tFTHG \tFTAG")
    for row in result:
        print(f"{row[0]} \t\t{row[1]} \t{row[2]}")
    con.commit()
    con.close()


def question3b():
    con=connection()
    cursor=con.cursor()
    print("\nTOTAL NUMBER OF GAMES WON BY EACH TEAM DURING 2016 IN DESCENDING ORDER OF NUMBER OF HOME GAMES")
    cursor.execute("SELECT COUNT(*), HomeTeam FROM Matches WHERE Season = 2016 AND FTR ='H' GROUP BY HomeTeam ORDER BY COUNT(*) DESC")
    result=cursor.fetchall()
    print('\nCount\t\tHomeTeam\n')
    for row in result:
        print(f"{row[0]} \t\t{row[1]}")
    con.commit()
    con.close()


def question3c():
    con=connection()
    cursor=con.cursor()
    print("\nFIRST 10 ROWS OF UNIQUE_TEAMS")
    cursor.execute("SELECT * FROM Unique_Teams ORDER BY Unique_Team_ID LIMIT 10")
    result=cursor.fetchall()
    print("\nTeam Name\t\tUnique_Team_ID")
    for row in result:
        print(f"{row[0]} \t\t{row[1]}")
    con.commit()
    con.close()


def question3d():
    con=connection()
    cursor=con.cursor()
    print("\nPRINTING MATCH ID, TEAM ID & TEAM NAME- WHERE")
    cursor.execute("SELECT tm.Match_ID, tm.Unique_Team_ID, ut.Unique_Team_ID, ut.TeamName FROM Teams_in_Matches AS tm, Unique_Teams AS ut WHERE tm.Unique_Team_ID = ut.Unique_Team_ID ORDER BY tm.Match_ID LIMIT 20")
    result=cursor.fetchall()
    for record in result:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Match_ID- {record['Match_ID']}""")
    

    print("\nPRINTING MATCH ID, TEAM ID & TEAM NAME- JOIN")
    cursor.execute("SELECT tm.Match_ID, tm.Unique_Team_ID, ut.Unique_Team_ID, ut.TeamName FROM Teams_in_Matches AS tm INNER JOIN Unique_Teams AS ut ON tm.Unique_Team_ID = ut.Unique_Team_ID ORDER BY tm.Match_ID LIMIT 20")
    results = cursor.fetchall()
    for record in results:
        print(f"""
                  Team Name- {record['TeamName']}, Unique_Team_ID- {record['Unique_Team_ID']}, Match_ID- {record['Match_ID']}""")
    con.commit()
    con.close()


def question3e():
    con=connection()
    cursor=con.cursor()
    print("\nJOIN UNIQUE_TEAMS & TEAMS TABLES- 1")
    cursor.execute("SELECT * FROM UNIQUE_TEAMS JOIN TEAMS ON TEAMS.TEAMNAME LIKE UNIQUE_TEAMS.TEAMNAME LIMIT 10")
    result=cursor.fetchall()
    for row in result:
        print("Unique ID:", row[1])
        print("TEAM Name:", row[0])
        print("Season:",row[2])
        print("Market value:", row[8], "\n")
    con.commit()
    con.close()


def question3f():
    con=connection()
    cursor=con.cursor()
    cursor.execute("SELECT * FROM UNIQUE_TEAMS JOIN TEAMS ON TEAMS.TEAMNAME LIKE UNIQUE_TEAMS.TEAMNAME LIMIT 5")
    result=cursor.fetchall()
    for row in result:
        print("Unique ID:", row[1])
        print("TEAM Name:", row[0])
        print("Avg age home:", row[5])
        print("Season:", row[2])
        print("Foreign players:", row[6], "\n")
    con.commit()
    con.close()


def question3g():
    con=connection()
    cursor=con.cursor()
    print("Displaying the highest Match_ID for each team that ends in a 'y' or a 'r'")
    cursor.execute("SELECT MAX(Match_ID),TEAMS_IN_MATCHES.UNIQUE_TEAM_ID,TEAMNAME FROM TEAMS_IN_MATCHES JOIN UNIQUE_TEAMS ON TEAMS_IN_MATCHES.UNIQUE_TEAM_ID =UNIQUE_TEAMS.UNIQUE_TEAM_ID WHERE (TEAMNAME lIKE '%y') OR(TEAMNAME LIKE '%r')GROUP BY TEAMS_IN_MATCHES.UNIQUE_TEAM_ID,UNIQUE_TEAMS.TEAMNAME")
    result = cursor.fetchall()
    for row in result:
        print("Unique ID:", row[1])
        print("TEAM Name:", row[2])
        print("Match ID:", row[0], "\n")    
    con.commit()
    con.close()





# Main Program

connection()
question1b()
question1c()
question1d()
question1e()
question2a()
question2b()
question2c()
question2d()
question2e()
question3a()
question3b()
question3c()
question3d()
question3e()
question3f()
question3g()

    
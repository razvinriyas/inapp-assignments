import sqlite3

conn = sqlite3.connect('Cars_new.db')
query = """CREATE TABLE CARS(Car_Name CHAR(60) NOT NULL,Owner_Name CHAR(100) NOT NULL)"""
conn.execute(query)
i=0
while i<10:
    c_name = input("Car name:").lower().capitalize()
    n_name = input("Owner name:").lower().capitalize()
    conn.execute("INSERT INTO CARS (Car_name, Owner_Name) " "VALUES(?, ?)", (c_name, n_name))
    i+=1
conn.commit()
print("Data Inserted")
print("\nCar Name, Owner Name")
cursor = conn.execute("SELECT * from CARS")
for row in cursor:
    print(row)
conn.close()

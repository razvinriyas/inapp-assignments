import random

class Pet:
    def _init_(self, species=None, name=""):
        self.name = name
        self.list = ['Dog', 'Cat', 'Horse', 'Hamster']
        if species not in self.list:
            raise ValueError(f"Species must be within this list {self.list}")
        else:
            self.species = species

    def _str_(self):
        if len(self.name) == 0:
            print(f"Species of: {self.species}, is unnamed")
        else:
            print(f"Species of: {self.species}, is already named {self.name}")


class Dog(Pet):
    def _init_(self, name="", chases="Cats"):
        super()._init_("Dog", name)
        self.name = name
        self.chases = chases

    def _str_(self):
        if len(self.name) == 0:
            print(f"Species of Dog, unnamed, chases {self.chases}")
        else:
            print(f"Species of Dog, named {self.name}, chases {self.chases}")


class Cat(Pet):
    def _init_(self, name="", hates="Dogs"):
        super()._init_("Cat", name)
        self.name = name
        self.hates = hates

    def _str_(self):
        if len(self.name) == 0:
            print(f"Species of Dog, unnamed, chases {self.hates}")
        else:
            print(f"Species of Dog, named {self.name}, chases {self.hates}")
            
#main()
D=[] #Dog objects
C=[] #Cat objects
for i in range(1,6):
    D[i]=Dog()
for i in range(1,4):
    C[i]=Dog()

species=input("Enter Species Name")

pets= {"Dog":[D[1]], "Dog": [D[2]], "Dog": [D[3]],"Dog": [D[4]],"Dog": [D[5]], "Cat":[C[1]], "Cat":[C[2]], "Cat":[C[3]]}
condition=True
while condition:
    for "Dog" in pets:
        dog_name=input("Enter the Dog name")
        pets["Dog"].append [{dog_name}]

    for "Cat" in pets:
        cat_name=input("Enter the Dog name")
        pets["Cat"].append [{cat_name}]

pet=Pet(species=species,name=dog_name)
Dog(Pet)
pet=Pet(species=species,name=cat_name)
Cat(Pet)
